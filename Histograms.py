import os
import pathlib

import matplotlib.pyplot as plt
import numpy as np
import scipy.linalg
import scipy.stats as scs

output_dir_name = 'plots'


def check_output_dir():
    output_dir = os.path.join(pathlib.Path(__file__).parent.absolute(), output_dir_name)
    if not os.path.isdir(output_dir):
        os.mkdir(output_dir)

    return output_dir


def fit_surface(x_in, y_in, z_in, steps=0.1):
    x_out, y_out = np.meshgrid(np.arange(min(x_in), max(x_in), steps), np.arange(min(y_in), max(y_in), steps))

    a = np.c_[x_in, y_in, np.ones(z_in.shape[0])]
    c, r, _, t = scipy.linalg.lstsq(a, z_in)

    z_out = c[0] * x_out + c[1] * y_out + c[2]

    return x_out, y_out, z_out


def get_surface_normal(surface_x, surface_y, surface_z):
    base_point = np.array([surface_x[0][0], surface_y[0][0], surface_z[0][0]])
    dir_point1 = np.array([surface_x[1][0], surface_y[1][0], surface_z[1][0]])
    dir_point2 = np.array([surface_x[0][1], surface_y[0][1], surface_z[0][1]])

    dir_vector1 = dir_point1 - base_point
    dir_vector2 = dir_point2 - base_point

    surface_normal = np.cross(dir_vector1, dir_vector2)
    return surface_normal / np.linalg.norm(surface_normal)


def import_data(path: str, delimiter=' '):
    data = np.genfromtxt(path, delimiter=delimiter)
    if data.shape[1] < 3:
        raise Exception(f'File {path} has not enough data.')

    data_x = data[:, 0]
    data_y = data[:, 1]
    data_z = data[:, 2]

    return data_x, data_y, data_z


def plot_histogram(data, name=None):
    fig, ax = plt.subplots(1, 1)
    _, bins, _ = ax.hist(data, bins='auto', density=True)
    plt.grid('True')

    if name is not None:
        plt.title(name)

    return fig, ax, bins


def create_histogram(path: str, name: str, show=False):
    output_dir = check_output_dir()
    output_path = os.path.join(output_dir, f'{name}.png')

    try:
        data_x, data_z, data_y = import_data(path)
    except Exception as ex:
        print(str(ex))
        return

    fitted_x, fitted_y, fitted_z = fit_surface(data_x, data_y, data_z)
    surface_normal = get_surface_normal(fitted_x, fitted_y, fitted_z)

    data_points = [data_x, data_y, data_z]
    fitted_surface_point = [fitted_x[0][0], fitted_y[0][0], fitted_z[0][0]]

    # calculate the distance between real data points and fitted surface
    distance_deviation = np.dot(surface_normal, data_points) - np.dot(fitted_surface_point, surface_normal)

    fig, ax, bins = plot_histogram(distance_deviation, name)

    std = np.std(distance_deviation)
    mean = np.mean(distance_deviation)
    gauss = scs.norm.pdf(bins, mean, std)

    plt.plot(bins, gauss, 'k', linewidth=2)

    text = f'Standardabweichung = {str(std.round(4))} mm \n Mittelwert = {mean:.3e} mm'
    ax.set_xlabel(text)
    plt.savefig(output_path)
    if show:
        plt.show()

    plt.close(fig)
