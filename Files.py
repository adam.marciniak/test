import os

from Histograms import create_histogram

files = []

for (path, dirnames, filenames) in os.walk("D:\\OR-Projects\\ray\\zuschnitte"):
    [create_histogram(os.path.join(path, file_name), str(file_name.split('.txt')[0]))
     for file_name in filenames]
